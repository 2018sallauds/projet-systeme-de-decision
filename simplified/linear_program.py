import numpy as np
from gurobipy import *
from mrsort_data_generator import *

def get_grades(n_criteria, n_instances, n_class):
    
    # Generates grades arrays from generator

    # Parameters :
    #   n_criterie (int) : number of criterias
    #   n_instances (int) : number of instances
    #   n_class (int) : number of classes

    # Returns :
    #   Accepted (array) : accepted instances of grades
    #   Refused (array) : refused instances of grades

    ground_truth = generate_ground_truth(n_criteria, n_class)
    grades, admission = generate_train_mrsort(ground_truth, n_instances)
    
    return grades, admission

def best_result_maxmin(grades, admission, n_criteria, n_class=2):

    # Return optimum parameters with max min model

    # Parameters :
    #   Accepted (array) : accepted instances of grades
    #   Refused (array) : refused instances of grades
    #   n_criterie (int) : number of criterias
    #   n_class (int) : number of classes

    # Returns :
    #   weights (array) : optimum weights
    #   lambda (float) : optimum threshold
    #   z (float) : optimum minimum sigma

    model = Model("PL modelling using matrix")

    weights = model.addMVar(shape = n_criteria, ub=1)
    lambda_ = model.addVar(ub=1, lb=0)
    frontiers=[]
    for i in range(n_class-1):
        frontiers.append(model.addMVar(vtype=GRB.INTEGER,shape=n_criteria, lb=0, ub=20))
    z = model.addVar(lb=-GRB.INFINITY)
    model.update()
    
    epsilon=0.01
    
    deltas=[]
    weights_student=[]
    sigmas = []
    M=100
    
    for n_boundary in range(n_class-1):
        if n_boundary>=1:
            for j in range(n_criteria):
                model.addConstr(frontiers[n_boundary].tolist()[j]>=frontiers[n_boundary-1].tolist()[j])
        Accepted=[]
        Refused=[]
        for i in range (len(admission)):
            if admission[i] == n_boundary+1:
                Accepted.append(grades[i])
            elif admission[i] == n_boundary:
                Refused.append(grades[i])

        for a in Accepted :
            deltas.append([])
            weights_student.append([])
            for i in range(n_criteria):
                deltas[-1].append(model.addVar(vtype=GRB.INTEGER,lb=0, ub=1))
                weights_student[-1].append(model.addVar(lb=0, ub=1))
                model.update()
                model.addConstr(M*(deltas[-1][-1]-1)<=a[i]-frontiers[n_boundary].tolist()[i])
                model.addConstr(M*(deltas[-1][-1])>=a[i]-frontiers[n_boundary].tolist()[i]+epsilon)
                model.addConstr(weights.tolist()[i]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]+weights.tolist()[i]-1<=weights_student[-1][-1])

            sigmas.append(model.addVar(lb=-GRB.INFINITY))
            model.update()
            model.addConstr(sum(weights_student[-1]) - lambda_ - sigmas[-1] == 0)
            model.addConstr(sigmas[-1] >= z)

        for r in Refused :

            deltas.append([])
            weights_student.append([])
            for i in range(n_criteria):
                deltas[-1].append(model.addVar(vtype=GRB.INTEGER,lb=0, ub=1))
                weights_student[-1].append(model.addVar(lb=0, ub=1))
                model.update()
                model.addConstr(M*(deltas[-1][-1]-1)<=r[i]-frontiers[n_boundary].tolist()[i])
                model.addConstr(M*(deltas[-1][-1])>=r[i]-frontiers[n_boundary].tolist()[i]+epsilon)
                model.addConstr(weights.tolist()[i]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]+weights.tolist()[i]-1<=weights_student[-1][-1])

            sigmas.append(model.addVar(lb=-GRB.INFINITY))
            model.update()
            model.addConstr(sum(weights_student[-1]) - lambda_ + sigmas[-1] == 0)
            model.addConstr(sigmas[-1] >= z)
        
    model.addConstr(sum(weights) == 1)
    model.update()

    model.setObjective(z, GRB.MAXIMIZE)
    model.params.outputflag = 0
    model.optimize()

    return weights.X, lambda_.x, model.objVal, [frontiers[i].X for i in range(n_class-1)]

def best_result_maxsum(grades, admission, n_criteria, n_class=2):

    # Return optimum parameters with max sum of sigmas model

    # Parameters :
    #   Accepted (array) : accepted instances of grades
    #   Refused (array) : refused instances of grades
    #   n_criterie (int) : number of criterias
    #   n_class (int) : number of classes

    # Returns :
    #   weights (array) : optimum weights
    #   lambda (float) : optimum threshold
    #   s (float) : optimum sum of sigmas

    model = Model("PL modelling using matrix")

    weights = model.addMVar(shape = n_criteria, ub=1)
    lambda_ = model.addVar(ub=1, lb=0)
    frontiers=[]
    for i in range(n_class-1):
        frontiers.append(model.addMVar(vtype=GRB.INTEGER,shape=n_criteria, lb=0, ub=20))
    model.update()

    sigmas = []
    
    epsilon=0.01
    deltas=[]
    weights_student=[]
    M=100
    
    for n_boundary in range(n_class-1):
        if n_boundary>=1:
            for j in range(n_criteria):
                model.addConstr(frontiers[n_boundary].tolist()[j]>=frontiers[n_boundary-1].tolist()[j])
        Accepted=[]
        Refused=[]
        for i in range (len(admission)):
            if admission[i] == n_boundary+1:
                Accepted.append(grades[i])
            elif admission[i] == n_boundary:
                Refused.append(grades[i])

        for a in Accepted :
            deltas.append([])
            weights_student.append([])
            for i in range(n_criteria):
                deltas[-1].append(model.addVar(vtype=GRB.INTEGER,lb=0, ub=1))
                weights_student[-1].append(model.addVar(lb=0, ub=1))
                model.update()
                model.addConstr(M*(deltas[-1][-1]-1)<=a[i]-frontiers[n_boundary].tolist()[i])
                model.addConstr(M*(deltas[-1][-1])>=a[i]-frontiers[n_boundary].tolist()[i]+epsilon)
                model.addConstr(weights.tolist()[i]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]+weights.tolist()[i]-1<=weights_student[-1][-1])
            sigmas.append(model.addVar(lb=-GRB.INFINITY))
            model.update()
            model.addConstr(sum(weights_student[-1]) - lambda_ - sigmas[-1] == 0)

        for r in Refused :
            deltas.append([])
            weights_student.append([])
            for i in range(n_criteria):
                deltas[-1].append(model.addVar(vtype=GRB.INTEGER,lb=0, ub=1))
                weights_student[-1].append(model.addVar(lb=0, ub=1))
                model.update()
                model.addConstr(M*(deltas[-1][-1]-1)<=r[i]-frontiers[n_boundary].tolist()[i])
                model.addConstr(M*(deltas[-1][-1])>=r[i]-frontiers[n_boundary].tolist()[i]+epsilon)
                model.addConstr(weights.tolist()[i]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]>=weights_student[-1][-1])
                model.addConstr(deltas[-1][-1]+weights.tolist()[i]-1<=weights_student[-1][-1])
            sigmas.append(model.addVar(lb=-GRB.INFINITY))
            model.update()
            model.addConstr(sum(weights_student[-1]) - lambda_ + sigmas[-1] == 0)
        
    model.addConstr(sum(weights) == 1)
    model.update()

    model.setObjective(sum(sigmas), GRB.MAXIMIZE)
    model.params.outputflag = 0
    model.optimize()
    
    return weights.X, lambda_.x, model.objVal, [frontiers[i].X for i in range(n_class-1)]

def get_class(grade, predicted_boundaries, predicted_lambda_, predicted_weights, n_criteria):
    state=0
    for boundary in predicted_boundaries:
        score=0
        for i in range(n_criteria):
            if grade[i]>=boundary[i]:
                score+=predicted_weights[i]
        if score>=predicted_lambda_:
            state+=1
    return state

def check_linear_answer(grades, admission, predicted_boundaries, predicted_lambda_, predicted_weights, n_criteria):
    nb_false = 0
    state = []

    for i in range (len(grades)):
        state.append(get_class(grades[i], predicted_boundaries, predicted_lambda_, predicted_weights, n_criteria))
        if state[i] != admission[i]:
            nb_false += 1

    return nb_false/len(admission)