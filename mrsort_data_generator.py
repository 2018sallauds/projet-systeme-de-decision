import random as rd
import numpy as np
import os
import math

def generate_ground_truth(n_criteria, n_class):

    # Generates a ground truth randomly

    # Parameters :
    #   n_criteria (int) : number of criterias
    #   n_class (int) : number of classes
    
    # Returns :
    #   ground_truth (dict) : dictionary containing the ground truth 
    #     {
    #         weights (array<int>) : array of weights,
    #         lambda (int) : lambda value,
    #         boundary (array<array<int>>) : lambda value,
    #     }

    ground_truth={}
    weights=[rd.randint(1,100) for i in range(n_criteria)]
    weights=[weight/sum(weights) for weight in weights]
    ground_truth['weights']=weights
    lambda_=rd.uniform(0,1)
    ground_truth['lambda']=lambda_
    
    frontieres=[[] for i in range(n_class-1)]
    for i in range(n_criteria):
        limit_bounds=np.sort(rd.sample([k for k in range(21)],n_class-1))
        for j in range(n_class-1):
            frontieres[j].append(limit_bounds[j])

    ground_truth['boundary']=frontieres
    
    print(">>> GROUND TRUTH GENERATED")
    for key, item in ground_truth.items():
        print(f'{key} : {item}')
    return ground_truth
    
def generate_train_mrsort(ground_truth, n_instances, noise=0):

    # Generates a training dataset randomly

    # Parameters :
    #   ground_truth (dict) : dictionary containing the ground_truth
    #   n_instances (int) : number of instances in the training set
    #   noise (float) (default 0) : value between 0 and 1 indicating the proportion of noise added to the dataset

    # Returns :
    #   grades (array) : matrix containing the grades (one line per instance and one column per criteria)
    #   admission (list) : list containing the admission results (1=accepted, 0=refused)

    n_criteria=len(ground_truth["weights"])
    grades=np.random.randint(0,20,size=(n_instances, n_criteria))
    
    def get_class(grade, boundaries, lambda_, weights):
        state=0
        for boundary in boundaries:
            score=0
            for i in range(n_criteria):
                if grade[i]>=boundary[i]:
                    score+=weights[i]
            if score>=lambda_:
                state+=1
        return state
    
    def add_noise(admission,n_class):
        a=np.random.randn(1)
        if a<0:
            return (admission+math.floor(a))%n_class
        elif a >=0:
            return (admission+math.ceil(a))%n_class
        
    admission=[get_class(grades[i],ground_truth["boundary"], ground_truth["lambda"], ground_truth["weights"]) for i in range(n_instances)]
    if noise>0 and noise<=1:
        sample=rd.sample(range(n_instances), int(noise*n_instances))
        for i in sample:
            admission[i]=(add_noise(admission[i],len(ground_truth["boundary"])+1))
    print(">>> TRAINING DATA GENERATED")
    return grades, admission

def save_mrsort_as_txt(ground_truth, grades, admission, path="data_mrsort.txt"):

    # Save a training dataset in txt format

    # Parameters :
    #   ground_truth (dict) : dictionary containing the ground_truth
    #   grades (array) : matrix containing the grades (one line per instance and one column per criteria)
    #   admission (list) : list conating the admission results (1=accepted, 0=refused)
    #   path (str) : path of the txt file created (should end with .txt)
    
    # Returns :
    #   None

    if not path.endswith(".txt"):
        print("Wrong path : should end with .txt")
        pass
    f=open(path,"w+")
    f.write(f'WEIGHTS :')
    for weight in ground_truth["weights"]:
        f.write(f' {weight}')
    f.write("\n")
    f.write(f'BOUNDARY : {len(ground_truth["boundary"])}\n')
    for boundary in ground_truth["boundary"]:
        for b in boundary:
            f.write(f'{b} ')
        f.write('\n')
    f.write(f'LAMBDA : {ground_truth["lambda"]}\n')
    f.write(f'ADMISSION :')
    for a in admission:
        f.write(f' {a}')
    f.write("\n")
    for i in range(grades.shape[0]):
        for j in range(grades.shape[1]):
            f.write(f'{grades[i][j]} ')
        f.write('\n')
    print(">>> TXT FILE CREATED")
    return None
    
    
def load_mrsort_as_txt(path='data_mrsort.txt'):

    # Load a training dataset from a txt file

    # Parameters :
    #   path (str) : path of the txt file

    # Returns :
    #   ground_truth (dict) : dictionary containing the ground_truth
    #   grades (array) : matrix containing the grades (one line per instance and one column per criteria)
    #   admission (list) : list conating the admission results (1=accepted, 0=refused)

    if not os.path.isfile(path):
        print("Wrong path : no file found")
        pass
    ground_truth={}
    grades=[]
    f=open(path, "r")
    f1=f.readlines()
    ground_truth["weights"]=[float(weight) for weight in f1.pop(0)[:-1].split(' ')[2:]]
    n_class=int(f1.pop(0)[11:])
    ground_truth["boundary"]=[]
    for i in range(n_class):
        ground_truth["boundary"].append([int(b) for b in f1.pop(0)[:-1].split(' ')[:-1]])
    ground_truth["lambda"]=float(f1.pop(0)[:-1].split(' ')[-1])
    admission=[int(a) for a in f1.pop(0)[:-1].split(' ')[2:]]
    for line in f1:
        grades.append([int(grade) for grade in line[:-1].split(' ')[:-1]])
    return ground_truth, np.array(grades), admission