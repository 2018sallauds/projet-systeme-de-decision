import numpy as np
import os
from mrsort_data_generator import *
from itertools import combinations
import csv
import subprocess


# conversion rang de l'élève, rang de l'évaluation => indice
def grade_to_index(index_student, index_criteria, index_class, n_criteria, n_instances):
    return index_class * n_criteria * n_instances + n_criteria * index_student + index_criteria + 1


# énumération des sous ensembles de critères sous forme de dictionnaire de la forme :
# {
#     index (int) : subset
# }
# avec index commençant à partir de n_instances * n_critera + 1 (suite de la dernière évaluation du dernier élève) (dans le cas de 2 classes uniquement)
def get_subsets(n_criteria, index_class, n_class, n_instances):
    res, index = {}, (n_class-1) * n_instances * n_criteria + index_class * (2**n_criteria - 1) + 1
    for i in range(n_criteria):
        for subset in combinations(range(1, n_criteria+1), i+1):
            res[index] = subset
            index += 1
    return res


#conversion du range de l'élève vers la variable objectif z
def student_to_goal(index_student, n_criteria, n_class, n_instances):
    idx0=(n_class-1) * n_instances * n_criteria + (n_class-1) * (2**n_criteria - 1) + 1
    return idx0+index_student


def get_clauses(grades, admission, n_instances, n_criteria, n_class):

    # Clauses 1 :
    clauses_1 =  []
    for c in range(n_class-1):
        for k in range(n_criteria):
            grade_tuples = [(i, grades[i][k]) for i in range(n_instances-1)]
            sorted_grade_tuples = sorted(grade_tuples, key=lambda student: student[1])
            for i in range(len(sorted_grade_tuples)-1):
                clauses_1.append([-grade_to_index(sorted_grade_tuples[i][0], k, c, n_criteria, n_instances), grade_to_index(sorted_grade_tuples[i+1][0], k, c, n_criteria, n_instances)])
                
    # Clauses 2 :
    clauses_2 = []
    for s in range(n_instances):
        for k in range(n_criteria):
            for c in range(n_class-1):
                for inf_c in range(c):
                    clauses_2.append([-grade_to_index(s, k, c, n_criteria, n_instances), grade_to_index(s, k, inf_c, n_criteria, n_instances)])
                    
    # Clauses 3 :
    clauses_3 = []
    subsets = [get_subsets(n_criteria, c, n_class, n_instances) for c in range(n_class-1)]
    for c in range(n_class-1):
        for i, subset_i in subsets[c].items():
            for j, subset_j in subsets[c].items():
                if set(subset_i).issubset(subset_j) and i != j:
                    clauses_3.append([-i, j])
                    
    # Clauses 4 :
    clauses_4 = []
    for c in range(n_class-1):
        for i, _ in subsets[c].items():
            for inf_c in range(c):
                clauses_4.append([-i,(i-(c-inf_c)*(2**n_criteria-1))])
                
    # Clauses 5 :
    # Clauses 6 :

    clauses_5 = []
    clauses_6 = []

    for c in range(n_class-1):
        for i, subset in subsets[c].items():
            for s in range(n_instances):
                admission_class=admission[s]
                if admission_class==c:
                    clauses_5.append([-grade_to_index(s, k-1, c, n_criteria, n_instances) for k in subset] + [-i] + [-student_to_goal(s, n_criteria, n_class, n_instances)])
                elif admission_class==c+1:
                    opposite_subset = [k for k in range(1, n_criteria+1) if k not in subset]
                    if opposite_subset!=[]:
                        idx0=(n_class-1) * n_instances * n_criteria + c * (2**n_criteria - 1)
                        clauses_6.append([grade_to_index(s, k-1, c, n_criteria, n_instances) for k in subset]+[i+(2**n_criteria-1)-2*(i-idx0)]+[-student_to_goal(s, n_criteria, n_class, n_instances)])
                    else:
                        clauses_6.append([grade_to_index(s, k-1, c, n_criteria, n_instances) for k in subset]+[-student_to_goal(s, n_criteria, n_class, n_instances)])

    clauses_goal=[]
    for s in range(n_instances):
        clauses_goal.append([student_to_goal(s, n_criteria, n_class, n_instances)])

    return clauses_1, clauses_2, clauses_3, clauses_4, clauses_5, clauses_6, clauses_goal


#Construction du DIMCS et Résolution

def clauses_to_dimacs(list_of_clauses, list_of_weights, numvar) :
    dimacs = 'c This is it\np wcnf '+str(numvar)+' '+str(sum([len(clauses) for clauses in list_of_clauses]))+'\n'
    for i in range(len(list_of_clauses)):
        for clause in list_of_clauses[i] :
            dimacs += str(list_of_weights[i]) + ' '
            for atom in clause :
                dimacs += str(atom) +' '
            dimacs += '0\n'
    return dimacs

def write_dimacs_file(dimacs, filename):
    with open(os.path.join(os.getcwd(), 'result', filename), "w", newline="") as wcnf:
        wcnf.write(dimacs)


#Attention à utiliser la vesion du solveur compatible avec votre système d'exploitation, mettre le solveur dans le même dossier que ce notebook        

def exec_gophersat(filename, n_criteria, n_instances, n_class, grades, cmd = "./gophersat.exe", encoding = "utf8") :

    result = subprocess.run([cmd, filename], stdout=subprocess.PIPE, check=True, encoding=encoding)
    string = str(result.stdout)
    lines = string.splitlines()
    subsets = [get_subsets(n_criteria, c, n_class, n_instances) for c in range(n_class-1)]

    if lines[2] == "s UNKNOWN":
        return -1, [], {}, []

    nb_unsatisfied_clauses = int(lines[1][2:])

    model = lines[3][3:].split(" ")
    model = [k.replace("x", "") for k in model if len(k)>0]

    res_grades = []
    for i in range(n_instances):
        res_grades.append([{grades[i][k]: [int(model[grade_to_index(i, k, c, n_criteria, n_instances)-1])>0 for c in range(n_class-1)]} for k in range(n_criteria)])

    res_valid_subsets = [[subset for i, subset in subsets[c].items() if int(model[i-1])>0] for c in range(n_class-1)]

    frontiers = []
    for c in range(n_class-1):
        class_frontier = []
        for k in range(n_criteria):
            above_frontier = [grades[i][k] for i in range(n_instances) if int(model[grade_to_index(i, k, c, n_criteria, n_instances)-1])>0]
            if len(above_frontier)>0:
                class_frontier.append(min(above_frontier))
            else:
                class_frontier.append(20)
        frontiers.append(class_frontier)

    return nb_unsatisfied_clauses, [int(x) for x in model if int(x) != 0], res_grades, res_valid_subsets, frontiers


def check_weighted_answer(res_grades, res_valid_subsets, admission):
    nb_false = 0

    for i in range(len(res_grades)):
        for c in range(len(res_valid_subsets)):
            subset = [k+1 for k in range(len(res_grades[i])) if list(res_grades[i][k].values())[0][c] > 0]
            if tuple(subset) not in res_valid_subsets[c] and admission[i] >= c+1:
                nb_false += 1

    return nb_false/len(admission)


def sat_weighted_resolve(grades, admission, n_instances, n_criteria, n_class):

    #Lancer la résolution

    clauses_1, clauses_2, clauses_3, clauses_4, clauses_5, clauses_6, clauses_goal = get_clauses(grades, admission, n_instances, n_criteria, n_class)
    
    list_of_clauses = [clauses_1, clauses_2, clauses_3, clauses_4, clauses_5, clauses_6, clauses_goal]
    list_of_weights = [10*n_instances, 10*n_instances, 10*n_instances, 10*n_instances, 10*n_instances, 10*n_instances, 1]
    myDimacs = clauses_to_dimacs(list_of_clauses, list_of_weights, (n_class-1) * (n_instances * n_criteria + 2**n_criteria - 1)+n_instances)

    write_dimacs_file(myDimacs,"workingfile.wcnf")
    res = exec_gophersat("./result/workingfile.wcnf", n_criteria, n_instances, n_class, grades)

    return res