import numpy as np
import os
from mrsort_data_generator import *
from itertools import combinations
import csv
import subprocess

# conversion rang de l'élève, rang de l'évaluation => indice
def grade_to_index(index_student, index_criteria, index_class, n_criteria, n_instances):
    return index_class * n_criteria * n_instances + n_criteria * index_student + index_criteria + 1


# énumération des sous ensembles de critères sous forme de dictionnaire de la forme :
# {
#     index (int) : subset
# }
# avec index commençant à partir de n_instances * n_critera + 1 (suite de la dernière évaluation du dernier élève) (dans le cas de 2 classes uniquement)
def get_subsets(n_criteria, index_class, n_class, n_instances):
    res, index = {}, (n_class-1) * n_instances * n_criteria + index_class * (2**n_criteria - 1) + 1
    for i in range(n_criteria):
        for subset in combinations(range(1, n_criteria+1), i+1):
            res[index] = subset
            index += 1
    return res


def get_clauses(grades, admission, n_instances, n_criteria, n_class):

    # Clauses 1 : si un élève a une évaluation au dessus d'une frontière, tous les autres élèves avec une évaluation supérieure sont au dessus de la frontière
    # => pour tout s, s' élèves, k évaluation, s_k < s'_k => non alpha_sk ou alpha_s'k

    clauses_1 =  []
    for c in range(n_class-1):
        for k in range(n_criteria):
            grade_tuples = [(i, grades[i][k]) for i in range(n_instances)]
            sorted_grade_tuples = sorted(grade_tuples, key=lambda student: student[1])
            for i in range(len(sorted_grade_tuples)-1):
                clauses_1.append([
                    -grade_to_index(sorted_grade_tuples[i][0], k, c, n_criteria, n_instances),
                    grade_to_index(sorted_grade_tuples[i+1][0], k, c, n_criteria, n_instances)
                ])

    # Clauses 1 bis : si un élève a une évaluation au dessus d'une frontière de classe c+1, alors cette évaluation est au dessus de la frontière de la classe c
    # => pour tout s élève, k évaluation, non alpha_sk(c+1) ou alpha_skc

    clauses_1_bis = []
    for c in range(n_class-2):
        for s in range(n_instances):
            for k in range(n_criteria):
                clauses_1_bis.append([
                    -grade_to_index(s, k, c+1, n_criteria, n_instances),
                    grade_to_index(s, k, c, n_criteria, n_instances)
                ])


    # Clauses 2 : si un ensemble de critères est suffisent, tout sur-ensemble est aussi suffisant
    # => pour tout C, C', si C inclu dans C', non beta_C ou beta_C'

    clauses_2 = []
    subsets = [get_subsets(n_criteria, c, n_class, n_instances) for c in range(n_class-1)]
    for c in range(n_class-1):
        for i, subset_i in subsets[c].items():
            for j, subset_j in subsets[c].items():
                if set(subset_i).issubset(subset_j) and i != j:
                    clauses_2.append([-i, j])

    # Clauses 2 bis : si un ensemble de critères est suffisant pour la classe c+1, il est suffisant pour la classe c
    # => pour tout C, non beta_C_c+1 ou beta_C_c

    clauses_2_bis = []
    for c in range(n_class-2):
        for i, _ in subsets[c].items():
            clauses_2_bis.append([-(i+2**n_criteria-1), i])

    # Clauses 3 et 4: tous les élèves acceptés (resp. non acceptés) ont leur ensemble d'évaluations au dessus de leur frontière qui est suffisant (resp. non suffisant)
    # clauses 3 => pour tout s, pour tout C=(c1,...,ci), beta_C ou alpha_sc1 ou ... ou alpha_sci ou non alpha_sc(i+1) ou ... ou non alpha_sn
    # clauses 4 => pour tout s, pour tout C=(c1,...,ci), non beta_C ou non alpha_sc1 ou ... ou non alpha_sci ou alpha_sc(i+1) ou ... ou alpha_sn

    clauses_3 = []
    clauses_4 = []
    for s in range(n_instances):
        admission_class = admission[s]
        if admission_class > 0:
            for i, subset in subsets[admission_class-1].items():
                opposite_subset = [k for k in range(1, n_criteria+1) if k not in subset]
                clauses_3.append(
                    [-grade_to_index(s, k-1, admission_class-1, n_criteria, n_instances) for k in subset] +
                    [grade_to_index(s, k-1, admission_class-1, n_criteria, n_instances) for k in opposite_subset] + [i]
                )
                if admission_class < n_class-1:
                    clauses_3.append(
                        [-grade_to_index(s, k-1, admission_class, n_criteria, n_instances) for k in subset] +
                        [grade_to_index(s, k-1, admission_class, n_criteria, n_instances) for k in opposite_subset] +
                        [-(i+2**n_criteria-1)]
                    )
            # au moins une évaluation est au dessus de la frontière
            clauses_3.append([grade_to_index(s, k, admission_class-1, n_criteria, n_instances) for k in range(n_criteria)])
        else:
            for i, subset in subsets[0].items():
                opposite_subset = [k for k in range(1, n_criteria+1) if k not in subset]
                clauses_4.append(
                    [-grade_to_index(s, k-1, 0, n_criteria, n_instances) for k in subset] +
                    [grade_to_index(s, k-1, 0, n_criteria, n_instances) for k in opposite_subset] +
                    [-i]
                )

    # print(f"grades : {grades}")
    # print(f"\nadmission : {admission}")

    return clauses_1, clauses_1_bis, clauses_2, clauses_2_bis, clauses_3, clauses_4


#Construction du DIMCS et Résolution

def clauses_to_dimacs(clauses,numvar) :
    dimacs = 'c This is it\np cnf '+str(numvar)+' '+str(len(clauses))+'\n'
    for clause in clauses :
        for atom in clause :
            dimacs += str(atom) +' '
        dimacs += '0\n'
    return dimacs

def write_dimacs_file(dimacs, filename):
    with open(os.path.join(os.getcwd(), 'result', filename), "w", newline="") as cnf:
        cnf.write(dimacs)


#Attention à utiliser la vesion du solveur compatible avec votre système d'exploitation, mettre le solveur dans le même dossier que ce notebook        

def exec_gophersat(filename, n_criteria, n_instances, n_class, grades, cmd = "./gophersat.exe", encoding = "utf8") :
    result = subprocess.run([cmd, filename], stdout=subprocess.PIPE, check=True, encoding=encoding)
    string = str(result.stdout)
    lines = string.splitlines()
    subsets = [get_subsets(n_criteria, c, n_class, n_instances) for c in range(n_class-1)]

    if lines[1] != "s SATISFIABLE":
        return False, [], {}, [], []

    model = lines[2][2:].split(" ")

    res_grades = []
    for i in range(n_instances):
        res_grades.append([{grades[i][k]: [int(model[grade_to_index(i, k, c, n_criteria, n_instances)-1])>0 for c in range(n_class-1)]} for k in range(n_criteria)])

    res_valid_subsets = [[subset for i, subset in subsets[c].items() if int(model[i-1])>0] for c in range(n_class-1)]

    frontiers = []
    for c in range(n_class-1):
        class_frontier = []
        for k in range(n_criteria):
            above_frontier = [grades[i][k] for i in range(n_instances) if int(model[grade_to_index(i, k, c, n_criteria, n_instances)-1])>0]
            if len(above_frontier)>0:
                class_frontier.append(min(above_frontier))
            else:
                class_frontier.append(20)
        frontiers.append(class_frontier)

    return True, [int(x) for x in model if int(x) != 0], res_grades, res_valid_subsets, frontiers


def check_answer(res_grades, res_valid_subsets, admission):
    nb_false = 0

    for i in range(len(res_grades)):
        for c in range(len(res_valid_subsets)):
            subset = [k+1 for k in range(len(res_grades[i])) if list(res_grades[i][k].values())[0][c] > 0]
            if tuple(subset) not in res_valid_subsets[c] and admission[i] >= c+1:
                nb_false += 1

    return nb_false/len(admission)


def sat_resolve(grades, admission, n_instances, n_criteria, n_class):

    #Lancer la résolution

    clauses_1, clauses_1_bis, clauses_2, clauses_2_bis, clauses_3, clauses_4 = get_clauses(grades, admission, n_instances, n_criteria, n_class)

    myClauses = clauses_1 + clauses_1_bis + clauses_2 + clauses_2_bis + clauses_3 + clauses_4
    myDimacs = clauses_to_dimacs(myClauses, (n_class-1) * (n_instances * n_criteria + 2**n_criteria - 1))

    # print(f"Nombre de clauses : {sum([len(k) for k in myClauses])}\n")

    write_dimacs_file(myDimacs,"workingfile.cnf")
    res = exec_gophersat("./result/workingfile.cnf", n_criteria, n_instances, n_class, grades)

    return res