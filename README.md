# Projet Système de décision

## Membres du groupe :

- Victor Daube
- Paola Grès
- Samuel Sallaud Pujade-Renaud

## Rappel du sujet :

Il s'agit dans ce projet d'apprendre le modèle de décision d'admission d'étudiants en fonction de leurs notes à une série d'examens.
Les paramètres à apprendre sont les suivants : 
- la **frontière** : la note permettant de valider chaque examen (il peut y avoir plusieurs notes par examen si le nombre de classers dans lesquelles sont placés les élèves est supérieur à deux)
- les **poids** : le coefficient de chaque examen
- **lambda** : la somme des coefficients des matières validées par les étudiants doit être supérieure ou égale à lambda pour être admis

Plusieurs méthodes différentes sont implémentées dans ce projet :
1. Un solveur par **programmation linéaire**
2. Un solveur **SAT**
3. Un solveur **MaxSAT pondéré**
4. Un solveur **SAT** pour critères *single-peaked*

Ces méthodes sont ensuites comparées selon leur temps de calcul, leur capacité de généralisation et leur aptitude à prendre en compte des données bruitées.

## Structure du Projet :

Ci-dessous les principaux fichiers du projet :
- `data/` : répertoire comportant les données au format `.txt` ou `.csv`
- `result/` : répertoire stockant les sorties du solveur `gophersat`, seulement en local
- `simplified` : répertoire avec les codes des solveurs des notebooks sous version python simplifiée pour être comparés entre eux
- `mrsort_data_generator.py` : générateur de données (critères monotones)
- `SAT_interval_data_generator.py` : générateur de données (critères single-peaked sous forme d'intervalle)
- le solveur linéaire : le notebook `linear_program.ipynb`
- le solveur SAT : le notebook `SAT_program.ipynb`
- le solveur weighted MaxSAT : le notebook `MaxSAT_program.ipynb`
- le solveur SAT single-peaked : le notebook `SAT_interval_program.ipynb`
- le comparateur des résultats des différents solveurs : le notebook `comparator.ipynb`


## Guide d'utilisation :

1. Pour utiliser le **solveur linéaire** :

Il faut run le notebook `linear_program.ipynb`. Dans ce notebook, le dataset `data/data.txt` est importé. Ce dataset a été généré grâce au fichier python `mrsort_data_generator.py` et a les caractéristiques suivantes :
- `n_class` (nombre de classes dans lesquelles sont placés les élèves) : 3
- `n_criteria` (nombre de matières) : 6
- `n_instances` (nombre d'élèves) : 1000

Avec le ground truth suivant :
- `weights : [0.1318181818181818, 0.2636363636363636, 0.10454545454545454, 0.11818181818181818, 0.2818181818181818, 0.1]`
- `lambda : 0.55`
- `boundary : [[12, 7, 9, 10, 7, 9], [18, 17, 13, 18, 19, 10]]`

2. Pour utiliser les différents **solveurs SAT** :

Pour utiliser le solveur gophersat pour représenter le modèle NCS, il faut placer le fichier d'application `gophersat` dans le dossier principal. Attention à utiliser la vesion du solveur compatible avec votre système d'exploitation (ex: `gophersat.exe` pour Windows 64). 

L'implémentation du problème Inv-NCS à l'aide du solveur SAT est effectuée dans le notebook `SAT_program.ipynb`.

L'implémentation du problème Inv-NCS à l'aide du solveur MaxSAT pondéré est effectuée dans le notebook `MaxSAT_program.ipynb`.

L'implémentation du problème Inv-NCS à critères single-peaked à l'aide du solveur SAT est effectuée dans le notebook `SAT_interval_program.ipynb`.

Pour chacun des solveurs, il faut indiquer dans la 3e cellule du notebook les variables suivantes :
- `gophersat_filepath` : chemin vers l'application gophersat (ex : `'./gophersat.exe'`)
- `filepath` : chemin du jeu de données à importer si nécessaire, sous forme de fichier `.txt` ou `.csv`
- `import_data` : `True` si on souhaite utiliser les données importées, `False` si on souhaite générer des données aléatoires, bruitées ou non 




## Modules à importer si besoin :

Librairies python :
- numpy
- gurobipy
- random
- os
- math
- csv
- itertools
